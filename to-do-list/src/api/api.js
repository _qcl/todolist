import axios from "axios";


const instance = axios.create({
  baseURL: 'http://localhost:8082'
})

export const getTodos = () => {
  return instance.get('/todos')
}

export const addTodos = (data) => {
  return instance.post('/todos', { text: data, done: false })
}

export const updateTodos = (data) => {
  console.log(data);
  return instance.put(`/todos/${data.id}`, { text: data.text, done: data.done })
}

export const deleteTodos = (data) => {
  return instance.delete(`/todos/${data}`)
}

const api = { getTodos, addTodos, updateTodos, deleteTodos }

export default api