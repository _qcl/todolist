import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import NotFoundPage from './componments/pages/NotFoundPage'
import store from './app/store'
import { Provider } from 'react-redux'
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import HelpPage from './componments/pages/HelpPage';
import DoneListPage from './componments/pages/DoneListPage';
import TodoList from './componments/TodoList';
import DoneDetailPage from './componments/pages/DoneDetailPage';

const root = ReactDOM.createRoot(document.getElementById('root'));
const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    children: [
      {
        index: true,
        element: <TodoList></TodoList>
      },
      {
        path: '/help',
        element: <HelpPage />
      },
      {
        path: '/done',
        element: <DoneListPage />
      },
      {
        path: '/done/:id',
        element: <DoneDetailPage />
      },
    ]
  },
  {
    path: '*',
    element: <NotFoundPage />
  }
])

root.render(
  <Provider store={store}>
    <RouterProvider router={router}></RouterProvider>
  </Provider>
);

