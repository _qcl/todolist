import axios from "axios";


const instance = axios.create({
    baseURL: 'https://64c0b6680d8e251fd1126405.mockapi.io/api'
})

export const getTodos = () => {
    return instance.get('/todos')
}

export const addTodos = (data) => {
    return instance.post('/todos',{text:data, done:false})
}

export const updateTodos = (data) => {
    return instance.put(`/todos/${data.id}`,{text: data.text, done: data.done})
}

export const deleteTodos = (data) => {
    return instance.delete(`/todos/${data}`)
}

const todoApis = {getTodos, addTodos, updateTodos, deleteTodos}

export default todoApis