import './index.css';
import { useNavigate } from "react-router-dom"

const DoneItem = ({ item }) => {

    const navigate = useNavigate()
    const handleNavigate = (id) => {
        navigate('/done/' + id)
    }

  return (
    <>
      <div className='item'  onClick={() => handleNavigate(item.id)}>
        <div className={"text"}>{item.text}</div>
      </div>
      
    </>
  )
}

export default DoneItem; 