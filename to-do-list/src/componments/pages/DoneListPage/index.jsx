import { useSelector } from "react-redux"
import DoneItem from "../../DoneItem"


const DoneListPage = () => {
  const doneTodoList = useSelector(state => state.todoList.todoList).filter((item) => item.done)


  return (
    <div>
      {doneTodoList.length > 0 ? doneTodoList.map((i) => (<DoneItem item={i} key={i.id} />)) : <h1>You haven't Done Yet!</h1>}
    </div>
  )
}

export default DoneListPage