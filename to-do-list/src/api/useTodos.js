import { useDispatch } from "react-redux"
import { initTodoList } from "../features/counter/counterSlice"
import api from "./api"


const useTodos = () => {
  const dispatch = useDispatch()

  const getTodos = async () => {
    const { data } = await api.getTodos()
    dispatch(initTodoList(data))
  }

  const insertTodos = async (data) => {
    await api.addTodos(data)
    getTodos()
  }

  const modifyTodos = async (data) => {
    await api.updateTodos(data)
    getTodos()
  }

  const removeTodos = async (data) => {
    await api.deleteTodos(data)
    getTodos()
  }

  return {
    getTodos, insertTodos, modifyTodos, removeTodos
  }
}

export default useTodos