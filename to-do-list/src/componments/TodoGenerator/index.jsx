import "./index.css"
import useTodos from "../../api/useTodos";

const TodoGenerator = () => {

  let text = ""
  const { insertTodos } = useTodos()

  const handleChange = (event) => {
    text = event.target.value
  }

  const handleClick = () => {
    if (text === "")
      alert("请输入待办事项")
    else
    insertTodos(text)
  }

  return (
    <div className='TodoGenerator'>
      <input className='input' onChange={handleChange}></input>
      <button className='button' onClick={handleClick}>Add</button>
    </div>
  )
}

export default TodoGenerator;