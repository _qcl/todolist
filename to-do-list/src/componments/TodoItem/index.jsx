
import './index.css';
import useTodos from '../../api/useTodos';
import { useState } from 'react';

const TodoItem = ({ item }) => {

  const { modifyTodos } = useTodos()
  const { removeTodos } = useTodos()

  const [isEdit, setIsEdit] = useState(false)
  const [text, setText] = useState("")
  const changeStatus = async () => {
    const data = { id: item.id, text: item.text, done: !item.done }
    modifyTodos(data)
  }

  const handledelete = async () => {
    removeTodos(item.id)
  }

  const handleUpdate = async () => {
    const data = { id: item.id, text: text, done: item.done }
    setIsEdit(false)
    modifyTodos(data)
  }

  return (
    <>
      <div className='item'>
        {isEdit ? <input className='inputtext' value={text} onChange={event => setText(event.target.value)}></input> :
          <div onClick={changeStatus} className={item.done ? "deletetext" : "text"}>{item.text}</div>}
        {isEdit ? <button onClick={handleUpdate}>comfirm</button> :
          <button onClick={() => { setIsEdit(true) }}>edit</button>}
        <button onClick={handledelete}>X</button>
      </div>
    </>
  )

}

export default TodoItem; 