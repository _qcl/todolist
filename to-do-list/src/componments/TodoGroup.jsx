import ToDoItem from "./TodoItem";
import { useSelector } from 'react-redux'

const TodoGroup = () => {
  const todoList = useSelector((state) => state.todoList.todoList)
  return (
    <div>
      {
        todoList.map((item) => {
          return <ToDoItem item={item} key={item.id}></ToDoItem>
        })
      }
    </div>
  );
}

export default TodoGroup;