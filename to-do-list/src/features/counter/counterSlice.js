import { createSlice } from '@reduxjs/toolkit'

export const counterSlice = createSlice({
  name: 'todoList',
  initialState: {
    todoList: [],
  },
  reducers: {
    initTodoList: (state, action) => {
      state.todoList = action.payload
    },
  },
})

export const { initTodoList } = counterSlice.actions

export default counterSlice.reducer