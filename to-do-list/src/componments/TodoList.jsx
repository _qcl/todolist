import TodoGroup from "./TodoGroup";
import TodoGenerator from "./TodoGenerator";
import { useEffect } from "react";
import useTodos from "../api/useTodos";

const TodoList = () => {
  const {getTodos} = useTodos()
  useEffect(() => {
    getTodos()
  },[])

  return (
    <div>
      <h2>Todo List</h2>
      <TodoGroup></TodoGroup>
      <TodoGenerator></TodoGenerator>
    </div>
  )
}

export default TodoList;