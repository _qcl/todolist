Objective:
During today's learning session, I focused on studying Router for implementing single-page transitions. Additionally, I explored how to utilize Axios and Promise for communication between the frontend and backend. I also practiced using a UI component library to modify styles.

Reflective:
While working on my assignments, I encountered numerous challenges, especially when dealing with conditional rendering in React. This particular aspect consumed a significant amount of my time and caused confusion.

Interpretive:
The difficulties I faced with React's conditional rendering have highlighted the importance of thoroughly understanding this concept. It is clear that further practice and exploration in this area will help solidify my comprehension and improve my efficiency.

Decisional:
Moving forward, I plan to seek additional resources, such as documentation or assistance from classmates and teachers, to gain a deeper understanding of conditional rendering in React. By doing so, I can enhance my problem-solving skills and optimize the time devoted to tackling similar challenges in the future.