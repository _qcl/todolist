import React, { useState } from 'react';
import { Menu } from 'antd';
import { useNavigate } from "react-router-dom"


const items = [
  {
    label: 'Home',
    key: '/',
  },
  {
    label: 'HelpPage',
    key: '/help',
  },
  {
    label: 'DoneListPage',
    key: '/done',
  },

];
const Navigate = () => {
  const navigate = useNavigate()
  const [current, setCurrent] = useState('/');
  const onClick = (e,) => {
    setCurrent(e.key);
    navigate(e.key)
  };
  return <Menu onClick={onClick} selectedKeys={[current]} mode="horizontal" items={items} />;
};
export default Navigate;