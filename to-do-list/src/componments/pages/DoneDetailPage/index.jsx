import { useParams } from "react-router-dom"
import { useSelector } from "react-redux"


const DoneDetailPage = () => {

  const { id } = useParams()
  const item = useSelector(state => state.todoList.todoList.find(item => item.id === id))
  return (
    <>
      <div>{id}</div>
      <div>{item.text}</div>
    </>
  )
}

export default DoneDetailPage